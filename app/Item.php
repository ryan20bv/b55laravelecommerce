<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function category(){
    	return $this -> belongsTo("\App\Category");
    }

    // feb 24.2020
    // many to many relationship
    public function orders(){
    	// return $this->belongsToMany("\App\Order");//if no pivot column only compose of the id
    	return $this->belongsToMany("\App\Order")->withPivot("quantity")->withTimeStamps();
    }
    // feb 24.2020
}
