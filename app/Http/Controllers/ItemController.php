<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Item;
use Auth;
use\App\Category;
use Session;


class ItemController extends Controller
{
    public function index(){
    	$items = Item::all();
    	return view('catalog',compact('items'));
    }
    public function additem(){
    	$categories = Category::all();
    	return view('adminviews.additem', compact('categories'));
    }

    // use request because of form
    public function store(Request $req){
    	// validate then capture then save then redirect
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required | numeric",
    		"category_id" => "required",
    		"imgPath" => "required | image | mimes:jpeg,png,jpg,gif,tiff,tif,webp,bitmap"
    	);

    	// dd($req);
    	// to validate needs two parameter
    	// this represent item controller
    	$this -> validate($req, $rules);
    	// dd($req);
    	$newItem = new Item;
    	$newItem->name = $req->name;
    	$newItem->description = $req->description;
    	$newItem->price = $req->price;
    	$newItem->category_id = $req->category_id;

    	// image handling new theory
    	$image = $req->file('imgPath');
    	$image_name = time().".".$image->getClientOriginalExtension();

    	$destination = "images/"; //corresponds to the public images directory
    	$image->move($destination, $image_name);
    	$newItem->imgPath = $destination.$image_name;
    	$newItem->save();
    	Session::flash("message", "$newItem->name has been added");
    	return redirect('/catalog');

    }
    public function destroy($id){
        $itemToDelete = Item::find($id);
        $itemToDelete->delete();
        // redirect is for actual route and mybugs is already created
        return redirect('/catalog');
    }

    public function edit($id){
        $item = Item::find($id);
        $categories = Category::all();
        return view('adminviews.edititem', compact("item", "categories"));
    }

    public function update($id, Request $req){
        $item = Item::find($id);
        $rules = array(
            "name" => "required",
            "description" => "required",
            "price" => "required | numeric",
            "category_id" => "required",
            "imgPath" => "image | mimes:jpeg,png,jpg,gif,tiff,tif,webp,bitmap",
            );
        $this->validate($req, $rules);


        $item->name = $req->name;
        $item->description = $req->description;
        $item->price = $req->price;
        $item->category_id = $req->category_id;
        // if user upload new image
        if($req->file('imgPath') != null){
            $image = $req->file('imgPath');
            $image_name = time().".".$image->getClientOriginalExtension();

            $destination = "images/"; //corresponds to the public images directory
            $image->move($destination, $image_name);
            $item->imgPath = $destination.$image_name; 
        }
        $item->save();
            Session::flash("message", "$item->name has been updated");
            return redirect('/catalog');

    }

    public function addToCart($id, Request $req){
        // check if there ia an existing session
        
        if(Session::has('cart')){
            $cart = Session::get('cart');

        }else{
            $cart = [];
        }
        // check if this is the first time we'll add an item to our cart
        if(isset($cart[$id])){
            $cart[$id] += $req->quantity;
        }else{
            $cart[$id] = $req->quantity;
        }
        // dd($cart);

        Session::put("cart", $cart);
        // flash a message
        //Find the item
        // use the name and the quantity in your flash 
        // ex 2 of item name succesfully added to cart
        $item = Item::find($id);
                 

            Session::flash("message", "$req->quantity of $item->name has been added to cart");
        
        return redirect()->back();
    }


    public function showCart(){
        // we will create a new array containing item name, price, quantity and subtotal
        // connect item to quantity in item_order
        $items = [];
        $total = 0;

        if(Session::has('cart')){
            $cart=Session::get('cart');
            // cart is in associative array
            foreach($cart as $itemId=>$quantity){
                $item = Item::find($itemId);
                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;
                $items[] = $item; //this is how to push
                $total += $item->subtotal;
            }
        }

        return view ('userviews.cart', compact('items','total'));
    }

    public function removeItem($id){
        // to unset
        // $cart = Session::get('cart');
        Session::forget("cart.$id");
        return redirect()->back();
        }

    public function emptyCart(){
        // to unset
        // $cart = Session::get('cart');
        Session::forget("cart");
        return redirect()->back();
        }

}
