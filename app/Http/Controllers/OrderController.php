<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use Auth;
use \App\Order;
use Session;

class OrderController extends Controller
{
	//feb 24, 2020
    public function checkout(){
    	// save the transaction in the ordet table
    	// make sure there is user log in
    	//populate our item_order;
    	if(Auth::user()){
    		$order = new Order; //instance of order model
    		$order->user_id = Auth::user()->id;
    		$order->status_id = 1; //pending
    		$order->payment_id = 1; //COD
    		$order->total = 0;
    		$order->save();

    		//many many table
    		$cart = Session::get('cart');
    		$total = 0;
    		foreach($cart as $itemId => $quantity){
    			//items() is the function made in order.php
    			$order->items()->attach($itemId, ["quantity"=>$quantity]);
    				$item = Item::find($itemId);
                	$item->quantity = $quantity;
                	$item->subtotal = $item->price * $quantity;
                	// $items[] = $item; //this is how to push
                	$total += $item->subtotal;
            }
            	$order->total = $total;
            	$order->save();

            Session::forget('cart');
            Session::flash('message', "Order succesfully placed");
            return redirect('/catalog');

    	}else{
    		return redirect('login');
    	}

    }

    public function showOrders(){
    	//find is for id; //All is for all details
    	$orders = Order::where('user_id', Auth::user()->id)->get();

    	return view('userviews.orders',compact('orders'));
    }
    //end feb 24, 2020
}
