@extends('layouts.app')
@section('content')
	<h1 class="text-center py-2">Cart Page</h1>
	{{-- for validation on pay via COD --}}
	@if($items != null)

	{{-- end for validation on pay via COD --}}
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Item Name:</th>
							<th>Item Price:</th>
							<th>Item Quantity:</th>
							<th>Item Subtotal:</th>
							<th></th>

						</tr>
					</thead>
					<tbody>
						@foreach($items as $item)
							<tr>
								<td>{{$item->name}}</td>
								<td>{{$item->price}}</td>
								<td>{{$item->quantity}}</td>
								<td>{{$item->subtotal}}</td>
								<td>
									{{-- fefb 24, 2020 --}}
									<form action="/removeitem/{{$item->id}}" method="POST" 	>
										@csrf
										@method('delete')
										<button class="btn btn-danger" type="submit">Remove</button>
										
									</form>
								</td>
							</tr>
						@endforeach
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>Total: {{$total}}</td>
							<td>
								{{-- <form action="/emptycart" method="POST">
										@csrf
										@method('delete')
										<button class="btn btn-danger" type="submit">Empty Cart</button>
										
								</form> --}}
								<a href="/emptycart" class="btn btn-danger">Empty</a>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<a href="/checkout" class="btn btn-secondary">Pay Via COD</a>
							</td>
							<td></td>
							<td></td>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
	@else
		<h2 class="text-center py-5">Cart is empty, Go Shopping!!!</h2>
	@endif
@endsection