@extends('layouts.app')
@section('content')
	<div class="container">
		<h1 class="text-center py-1">CATALOG PAGE</h1>
	
		@if(Session::has("message"))
			<h4>{{Session::get('message')}}</h4>
		@endif
		<div class="row">
			@foreach($items as $indiv_item)
				<div class="col-lg-4 my-1">
					<div class="card">
						<img src="{{$indiv_item->imgPath}}" class="card-img-top border" alt="Nothing" height="250px">
						<div class="card-body">
							<h4 class="card-title">{{$indiv_item->name}}</h4>
							<p class="card-text">Price: Php {{ $indiv_item->price }}	</p>
							{{-- {{ $indiv_bug->category_id }} --}}
							<p class="card-text">{{ $indiv_item->description }}	</p>
							{{-- <p class="card-text">{{ $indiv_item->imgPath }}	</p> --}}
							<p class="card-text">Category :{{ $indiv_item->category->name}}	</p>
						</div>
					</div>
					<div class="card-footer d-flex">
						<form method="POST" action="/deleteitem/{{$indiv_item->id}}">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger ">Delete</button>
						</form>
						<a href="/edititem/{{$indiv_item->id}}" class="btn btn-info">Edit</a>
					
					</div>
					<div class="card-footer">
						<form action="/addtocart/{{$indiv_item->id}}" method="POST">
							@csrf
							<input type="number" name="quantity" class="form-control" value="1">
							<button class="btn btn-secondary btn-block" type="submit">Add to cart</button>
						</form>
					</div>
				</div>
			@endforeach
		</div>

	</div>


@endsection
