<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('statuses')->delete();
        \DB::table('statuses')->insert(array(
        	0=>
        	array(
        		'id'=> 1,
        		'name'=>'Pending',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	1=>
        	array(
        		'id'=> 2,
        		'name'=>'Processing',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	2=>
        	array(
        		'id'=> 3,
        		'name'=>'Cancelled by user',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	3=>
        	array(
        		'id'=> 4,
        		'name'=>'Cancelled by admin',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	4=>
        	array(
        		'id'=> 5,
        		'name'=>'Delivered',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	)
        
        ));
    }
}
