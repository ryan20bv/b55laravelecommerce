<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->delete();
        \DB::table('categories')->insert(array(
        	0=>
        	array(
        		'id'=> 1,
        		'name'=>'Bikes',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	1=>
        	array(
        		'id'=> 2,
        		'name'=>'Sunglases',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	2=>
        	array(
        		'id'=> 3,
        		'name'=>'Shoes',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	       	
        	)
        ));
    }
}
