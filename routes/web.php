<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog', 'ItemController@index');

// admin vies
Route::get('/additem', 'ItemController@additem');
Route::post('/additem', 'ItemController@store');
Route::delete('/deleteitem/{id}', 'ItemController@destroy');
Route::get('/edititem/{id}', 'ItemController@edit');
// to save the edited item
Route::Patch('/edititem/{id}', 'ItemController@update');
// cart crud
Route::post('/addtocart/{id}', 'ItemController@addToCart');

Route::get('/cart', 'ItemController@showCart');

Route::delete('/removeitem/{id}', 'ItemController@removeItem');

// Route::delete('/emptycart', 'ItemController@emptyCart');
Route::get('/emptycart', 'ItemController@emptyCart');

Route::get('/checkout', 'OrderController@checkOut');

Route::get('/showorders', 'OrderController@showOrders');
